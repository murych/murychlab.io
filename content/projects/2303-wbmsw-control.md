---
title: WB MSW Control
date: 2023-02-16
tags:
  - qt
  - modbus
---

- исходники: https://gitlab.com/murych/wb-msw-control

## описание

Dashboard application to configure and monitor Wirenboard WB-MSW sensor.

## зависимости

- Qt6: Core, SerialBus, Qml, QuickControls2
- gsl-lite
- gtest

## ресурсы

- [описание устройства](https://wirenboard.com/ru/product/wb-msw-v3/)
