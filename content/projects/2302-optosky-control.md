---
title: Optosky ATP1010 Control
date: 2022-10-04
tags:
  - qt
  - usb
  - spectrometry
---

- исхдоники https://gitlab.com/murych/optosky-atp1010-control

## описание

![device](https://optosky.com/image/cache/catalog/ATP1010/01-600x600.jpg)

## зависимости

## ресурсы

- [device description](https://optosky.com/uv-enhanced-mini-spectrometer.html)
- [device datasheet](https://optosky.com/image/catalog/pdf/ATP1010_UV%20enhanced%20Miniature%20Spectrometer_v2.1.pdf)
